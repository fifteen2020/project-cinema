[toc]
# client-cinema

### 一、项目架构
在前端，我们采用vue-cli 3的架构来进行开发

#### 1.1 项目目录
1. package-lock.json  -->项目及工具的依赖配置文件
2. README.md     -->项目说明文件
3. src    -->项目核心文件

#### 1.2 src目录结构

1. **assets** -->静态资源(存放css、js、img文件)
2. **components** -->公共组件存放(子组件)
3. **router** -->路由(配置项目路由)
4. **store**   -->状态管理文件存放
5. **views**   -->视图，存放**页面级**组件(父组件)
6. **views/Home.vue**  -->主要呈现在页面的组件
7. **App.vue** -->根组件，里面可添加路由
8. **main.js** -->入口文件，引入vue模块与路由以及一系列我们需要的模块

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
