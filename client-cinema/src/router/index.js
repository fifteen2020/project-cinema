 

// 解决导航栏或者底部导航tabBar中的vue-router在3.0版本以上频繁点击菜单报错的问题。
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location) {
    return originalPush.call(this, location).catch(err => err)
}
 
 
 
import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home";
import Login from "../views/login.vue";
import cinema_details from "../views/cinema_details.vue";
import cinema from "../views/cinema.vue";
import comic from "../views/comic.vue"
import tv_play from "../views/tvplay.vue"
import variety from "../views/variety.vue"
// 109
import ticket from "../views/ticket.vue"
import { color } from "echarts";
Vue.use(VueRouter);
 
 

//懒加载
let admin = () => import("@/views/admin/admin");
//用户
let User = () => import("@/components/admin/User");
//管理员
let tabledel = () => import("@/components/admin/Administrator");

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },

  {
 
    path: "/login",
    name: "login",
    component: Login,
  },
  {
    path: "/cinema_details",
    name: "cinema_details",
    component: cinema_details,
 
  },

  {
    path: "/cinema",
    name: "cinema",
    component:cinema,
  },
  {
    path: "/comic",
    name: "comic",
    component:comic,
  },
  {
    path: "/tv_play",
    name: "tv_play",
    component:tv_play,
  },
  {
    path: "/cinema_details",
    name: "cinema_details",
    component:cinema_details,
  },
  {
    path: "/variety",
    name: "variety",
    component:variety,
  },
  {
    path: "/ticket",
    name: "ticket",
    component:ticket
  },
  {
    path: "/admin",
    name: "admin",
    component: admin,
    //嵌套路由
    children: [
      //重定向,设置默认路由
      {
        path: 'zonyitop',
        component: () => import("@/components/admin/zonyitop")

      },
      {
        path: "",
        redirect: "default",
      },
      {
        path: "default",
        component: () => import("@/components/admin/default"),
      },
      {
        path: "User",
        component: () => import("@/components/admin/User"),
      },
      {
        path: "Administrator",
        component: () => import("@/components/admin/Administrator"),
      },
      {
        path: "cinemaimg",
        component: () => import("@/components/admin/cinema_imgList"),
      },
      {
        path: "cinematop",
        component: () => import("@/components/admin/cinematop"),
      },
      {
        path: "zonyitop",
        component: () => import("@/components/admin/zonyitop"),
      },
      {
        path: "Userplun",
        component: () => import("@/components/admin/Userplun"),
      },
      {
        path: "history",
        component: () => import("@/components/admin/history"),
      },
    ],

  },
  {
    path:"/personae",
    component:() => import("../views/personae.vue")
  },
  {
    path:"/adminlogin",
    component:() => import("../views/admin/adminlogin.vue")
  },

];
const router = new VueRouter({
  routes,
});
export default router;
