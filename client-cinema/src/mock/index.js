import Mock from 'mockjs'
import homeApi from "./home";

Mock.mock('/api/home/getDate', 'get', homeApi.getHomeDate)