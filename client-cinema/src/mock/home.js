import Mock from 'mockjs'
let List = [];
export default {
    getHomeDate: () => {
        for (let i = 0; i < 7; i++) {
            List.push(
                Mock.mock({
                    电视剧: Mock.Random.float(100, 8000, 0, 2),
                    电影: Mock.Random.float(100, 8000, 0, 2),
                    动漫: Mock.Random.float(100, 8000, 0, 2),
                    综艺: Mock.Random.float(100, 8000, 0, 2),
                    纪录片: Mock.Random.float(100, 8000, 0, 2),
                    游戏: Mock.Random.float(100, 8000, 0, 2),
                    其他: Mock.Random.float(100, 8000, 0, 2)
                })
            )
        }
        return {
            code: 20000,
            body: {
                // 饼图
                mallData: [
                    {
                        name: ' 电视剧',
                        value: Mock.Random.float(1000, 9999, 0, 2)
                    }, {
                        name: '电影',
                        value: Mock.Random.float(5000, 15000, 0, 2)
                    }, {
                        name: '动漫',
                        value: Mock.Random.float(1000, 9999, 0, 2)
                    }, {
                        name: '综艺',
                        value: Mock.Random.float(1000, 9999, 0, 2)
                    }, {
                        name: '纪录片',
                        value: Mock.Random.float(1000, 5000, 0, 2)
                    }, {
                        name: ' 游戏',
                        value: Mock.Random.float(1000, 9999, 0, 2)
                    },
                    {
                        name: ' 其他',
                        value: Mock.Random.float(1200, 7000, 0, 2)
                    }

                ],
                // 柱状图
                userData: [
                    {
                        date: '周一',
                        new: Mock.Random.integer(1, 100),
                        active: Mock.Random.integer(100, 1000)
                    },
                    {
                        date: '周二',
                        new: Mock.Random.integer(1, 100),
                        active: Mock.Random.integer(100, 1000)
                    },
                    {
                        date: '周三',
                        new: Mock.Random.integer(1, 100),
                        active: Mock.Random.integer(100, 1000)
                    },
                    {
                        date: '周四',
                        new: Mock.Random.integer(1, 100),
                        active: Mock.Random.integer(100, 1000)
                    },
                    {
                        date: '周五',
                        new: Mock.Random.integer(1, 100),
                        active: Mock.Random.integer(100, 1000)
                    },
                    {
                        date: '周六',
                        new: Mock.Random.integer(1, 100),
                        active: Mock.Random.integer(100, 1000)
                    },
                    {
                        date: '周日',
                        new: Mock.Random.integer(1, 100),
                        active: Mock.Random.integer(100, 1000)
                    }
                ],
                // 折线图
                orderData: {
                    date: [
                        '2020-6-1',
                        '2020-7-1',
                        '2020-8-1',
                        '2020-9-1',
                        '2020-10-1',
                        '2020-11-1',
                        '2020-12-1',
                    ],
                    data: List
                },
                tableData: [
                    {
                        name: '电视剧',
                        todayBuy: Mock.Random.float(100, 999, 0, 2),
                        monthBuy: Mock.Random.float(3000, 9999, 0, 2),
                        totalBuy: Mock.Random.float(10000, 99999, 0, 2)
                    },
                    {
                        name: '电影',
                        todayBuy: Mock.Random.float(500, 2000, 0, 2),
                        monthBuy: Mock.Random.float(3000, 5000, 0, 2),
                        totalBuy: Mock.Random.float(40000, 1000000, 0, 2)
                    },
                    {
                        name: '动漫',
                        todayBuy: Mock.Random.float(500, 2000, 0, 2),
                        monthBuy: Mock.Random.float(5000, 10000, 0, 2),
                        totalBuy: Mock.Random.float(40000, 1000000, 0, 2)
                    },
                    {
                        name: '综艺',
                        todayBuy: Mock.Random.float(100, 1000, 0, 2),
                        monthBuy: Mock.Random.float(3000, 5000, 0, 2),
                        totalBuy: Mock.Random.float(40000, 1000000, 0, 2)
                    },
                    {
                        name: '纪录片',
                        todayBuy: Mock.Random.float(100, 1000, 0, 2),
                        monthBuy: Mock.Random.float(3000, 5000, 0, 2),
                        totalBuy: Mock.Random.float(40000, 1000000, 0, 2)
                    },
                    {
                        name: '游戏',
                        todayBuy: Mock.Random.float(100, 1000, 0, 2),
                        monthBuy: Mock.Random.float(3000, 5000, 0, 2),
                        totalBuy: Mock.Random.float(40000, 1000000, 0, 2)
                    },
                    {
                        name: '其他',
                        todayBuy: Mock.Random.float(100, 1000, 0, 2),
                        monthBuy: Mock.Random.float(3000, 5000, 0, 2),
                        totalBuy: Mock.Random.float(40000, 1000000, 0, 2)
                    }
                ]
            }
        }
    }
}