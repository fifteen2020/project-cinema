
export default {
  data() {
    return {
      Del: false,
      currentPage1: 5,
      currentPage2: 5,
      currentPage3: 5,
      currentPage4: 4,
      //用户列表
      tableData: [],
      search: '',
      imgurl: '',
      checkBoxData: [],
    }
  },
  created() {
    //用户数据显示
    // axios.get("http://localhost:8082/cinemaall")
    //关于更新，可以将请求封装为一个函数，然后使用定时器来调用
    // this.show();
    // const loading = this.$loading({
    //   lock: true,
    //   text: 'Loading',
    //   spinner: 'el-icon-loading',
    //   background: 'rgba(0, 0, 0, 0.7)'
    // });
    // const timer = setInterval(() => {
    //   loading.close();
    // }, 1000 * 20);
    this.show();


    // this.$once('hook:beforeDestroy', () => {
    //   clearInterval(timer);
    // })
  },

  methods: {
    //获取选中值
    changeFun(val) {
      //遍历
      val.forEach(item => {
        this.checkBoxData.push(item.userid);
        
      })
      for (let i = 0; i < this.checkBoxData.length; i++) {

      }
      //Set 去重复

      let a = new Set(this.checkBoxData);
      this.checkBoxData = [...a];
      console.log(this.checkBoxData);
    },

    //显示用户数据
    show() {
      axios.get("http://localhost:8082/cinemaall")
        .then(response => {
          this.tableData = response.data;
          for (let i = 0; i < this.tableData.length; i++) {
            if (this.tableData[i].userimg == null) {
              this.tableData[i].userimg = "53ddbb8f-ce59-4fe0-9fdc-72dd20a8dcfcimg_1.png";
            } else if (this.tableData[i].userimg != null) {
            }
          }
        }).catch(eror => {
          console.log("失败");
        })
    },
    handleEdit(index, row) {
      console.log(index, row);
    },
    handleDelete(index, row) {
      console.log(index, row);
    },

    //分页
    handleSizeChange(val) {
      console.log(`每页 ${val} 条`);
    },
    handleCurrentChange(val) {
      console.log(`当前页: ${val}`);
    },
    open() {
      this.$confirm('确定要删除选中行的数据吗?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        axios.delete("http://localhost:8082/cinemaidList", {
          params: {
            Userid: this.checkBoxData + ''
          }
        }).then(resp => {
          console.log(this.userid);
          this.$notify.success({
            title: 'Info',
            message: '删除成功',
            showClose: false
          });
          this.show();

        }).catch(error=>{
          this.$notify.error({
            title: '错误',
            message: '发生错误，是否已选中',
            showClose: false
          });
        })

      }).catch(() => {
        this.$message({
          type: 'info',
          message: '已取消删除'
        });
      });
    },
    select(selection, row) {
      console.log(selection.length);
    }

  },
  watch: {
    data() {
      console.log("变化");
    }
  },
}