package org.example.entity;

public class allList {
    private int cinemaid;
    private String name;
    private String img;
    private String video;
    private String main;
    private String type;
    private String videotype;
    private String theme;
    private String subheading;
    private String time;
    private String swift;
    private String grade;

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public int getCinemaid() {
        return cinemaid;
    }

    public allList(String name, String img, String video, String main, String type, String videotype, String theme, String subheading, String time, String swift) {
        this.name = name;
        this.img = img;
        this.video = video;
        this.main = main;
        this.type = type;
        this.videotype = videotype;
        this.theme = theme;
        this.subheading = subheading;
        this.time = time;
        this.swift = swift;
    }

    public void setCinemaid(int cinemaid) {
        this.cinemaid = cinemaid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVideotype() {
        return videotype;
    }

    public void setVideotype(String videotype) {
        this.videotype = videotype;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getSubheading() {
        return subheading;
    }

    public void setSubheading(String subheading) {
        this.subheading = subheading;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSwift() {
        return swift;
    }

    public void setSwift(String swift) {
        this.swift = swift;
    }
}
