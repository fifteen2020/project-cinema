package org.example.entity;

public class SuperAdmin {
    private Integer adminid;
    private String User;
    private Integer password;
    private String adminimg;

    public Integer getAdminid() {
        return adminid;
    }

    public void setAdminid(Integer adminid) {
        this.adminid = adminid;
    }

    public String getUser() {
        return User;
    }

    public void setUser(String user) {
        User = user;
    }

    public Integer getPassword() {
        return password;
    }

    public void setPassword(Integer password) {
        this.password = password;
    }

    public String getAdminimg() {
        return adminimg;
    }

    public void setAdminimg(String adminimg) {
        this.adminimg = adminimg;
    }
}
