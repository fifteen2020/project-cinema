package org.example.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.example.entity.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Repository
@Mapper
public interface CinemaDao {
    //用户添加
   boolean UserAdd(User user);
    //用户登录匹配账号密码
    List<User> Userlogin(@Param("Username") String Username,@Param("password") Integer password);
    //根据id查询用户信息
    User Userid(@Param("Userid") Integer Userid);
    //图片上传
    void Userimgpush(@Param("Username") String Username,@Param("Userimg") String Userimg);
    //用户显示
    List<User> Userall();
    //用户修改
    boolean UserUp(User user);
   //批量删除
   boolean UserDelList(@Param("Userid") List<Integer> idList);

    //电影、动画..表显示
    List<allList> videoAll();

    //电影、动漫修改
    boolean allListAdd(allList allList);

 //后台用户登录匹配账号密码
  List<SuperAdmin> adminlogin(@Param("User") String User,@Param("password")Integer password);

  //管理员表显示
 List<SuperAdmin> adminshow();

    //根据id查询视频
    allList allListshow(@Param("cinemaid") Integer cinemaid);

    //用户评论
    List<Comments> Comment();

    //评论添加
    boolean Commentplun(Commentsplun commentsplun);

    //模糊查询
    List<allList> alllistshowtype(String type,String videotype);

    //添加历史记录
    boolean history(history history);

    //查询历史记录
    List<history2> historyshowdata();

    //根据用户id查询历史记录
    List<history2> historyshow(Integer Userid);

    //根据姓名，电影名查询历史记录
    List<history2> historyshow2(@Param("Username") String username,@Param("name") String name);

    //根据名称、类型查询影片
    List<allList>  cinemaallshow(@Param("name")String name);

    //id删除评论
    boolean plunid(@Param("id") Integer id);

 //热门推荐，显示12条数据
// List<allList> cinemalim();

}
