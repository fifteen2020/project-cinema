package org.example.service;

import org.example.entity.*;
import org.example.mapper.CinemaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CinemaServiceImp implements CinemaService{
    @Autowired
    private CinemaDao cinemaDao;

//    @Override
//    public boolean UserAdd(User user) {
//        return cinemaDao.UserAdd(user);
//    }

    @Override
    public boolean UserAdd(User user) {
        return cinemaDao.UserAdd(user);
    }

    @Override
    public List<User> Userlogin(String Username, Integer password) {

        return cinemaDao.Userlogin(Username,password);
    }

    @Override
    public User Userid(Integer Userid) {
        return cinemaDao.Userid(Userid);
    }

    @Override
    public void Userimgpush(String Username, String Userimg) {
        cinemaDao.Userimgpush(Username,Userimg);
    }

    @Override
    public List<User> Userall() {
        return cinemaDao.Userall();
    }

    @Override
    public boolean UserUp(User user) {
        return cinemaDao.UserUp(user);
    }

    @Override
    public boolean UserDelList(List<Integer> idList) {
        return cinemaDao.UserDelList(idList);
    }

    @Override
    public List<allList> videoAll() {
        return cinemaDao.videoAll();
    }

    @Override
    public boolean allListAdd(allList allList) {
        return cinemaDao.allListAdd(allList);
    }

    @Override
    public List<SuperAdmin> adminlogin(String User, Integer password) {
        return cinemaDao.adminlogin(User,password);
    }

    @Override
    public List<SuperAdmin> adminshow() {
        return cinemaDao.adminshow();
    }

//    @Override
//    public List<allList> cinemalim() {
//        return cinemaDao.cinemalim();
//    }

    @Override
    public allList allListshow(Integer cinemaid) {
        return cinemaDao.allListshow(cinemaid);
    }
    @Override
    public List<Comments> comment() {
        return cinemaDao.Comment();
    }

    @Override
    public boolean Commentplun(Commentsplun commentsplun) {
        return cinemaDao.Commentplun(commentsplun);
    }

    @Override
    public boolean history(history history) {
        return cinemaDao.history(history);
    }

    @Override
    public List<history2> historyshow(Integer Userid) {
        return cinemaDao.historyshow(Userid);
    }

    @Override
    public List<history2> historyshow2(String Username, String name) {
        return cinemaDao.historyshow2(Username,name);
    }

    @Override
    public List<history2> historyshowdata() {
        return cinemaDao.historyshowdata();
    }

    @Override
    public List<allList> alllistshowtype(String type, String videotype) {
        return cinemaDao.alllistshowtype(type,videotype);
    }

    @Override
    public List<allList> cinemaallshow(String name) {
        return cinemaDao.cinemaallshow(name);
    }

    @Override
    public boolean plunid(Integer id) {
        return cinemaDao.plunid(id);
    }


}
