package org.example.Controller;

import com.sun.org.apache.xpath.internal.operations.Mult;
import org.example.entity.*;
import org.example.service.CinemaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;
import java.util.UUID;


//输出json格式
@RestController
public class CinemaController {
    //交给容器处理

    @Autowired
    private CinemaService cinemaService;

    @CrossOrigin
    @PostMapping("/cinemaAdd")
    public boolean UserAdd( User user){
//        System.out.println(user);
            return cinemaService.UserAdd(user);
    }

    //跨域
    @CrossOrigin
    //登录
    @GetMapping("/cinemaregister")
    public List<User> register(String Username, Integer password){
            System.out.println(Username+"--"+password);
        return cinemaService.Userlogin(Username,password);
    }

    @CrossOrigin
    @PostMapping("/cinemaImgpush")
    public String Imgpush(String Username,@RequestParam("file") MultipartFile file,String Userimg) throws IOException {
            String filePath;
            User user = new User();

            String originalFileename = file.getOriginalFilename();
            String filea = originalFileename.substring(originalFileename.lastIndexOf(".")+1);
            if (filea.equals("mp4")){
                filePath = "D:\\cinema-stockpile\\mp4";
            }else {
                filePath = "D:\\cinema-stockpile\\imgs";
            }
        System.out.println(filea);
            String newFileName = UUID.randomUUID()+originalFileename;
        File targetFile = new File(filePath,newFileName);
        file.transferTo(targetFile);

        Userimg = newFileName;
        System.out.println(Username);
        cinemaService.Userimgpush(Username,Userimg);

        return "成功";

    }
    @CrossOrigin
    @GetMapping("/cinemaImgshow")
    public void cinemaImgshow(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String filename = req.getParameter("filename");

//        System.out.println(filename);

        String a = filename.substring(filename.lastIndexOf(".") + 1);
        String showu;
//        System.out.println(a);

        if (a.equals("mp4")){
            showu="D:\\cinema-stockpile\\mp4\\";
        }else {
            showu = "D:\\cinema-stockpile\\imgs\\";
        }
        String mimeType = req.getServletContext().getMimeType(filename);//获取文件的何种格式信息
        if (mimeType == null) {
            mimeType = "application/octet-stream";//如果没有获取到，那么以二进制格式
        }
        resp.setContentType(mimeType);

        String encodedFilename = URLEncoder.encode(filename, "UTF-8") ;//这个是防止文本文件等的下载后乱																					码问题
        resp.setHeader("Content-Disposition",  "attachment;filename="+encodedFilename);//attachment是表示下载时有提示存储地址弹窗

        InputStream in = new FileInputStream(showu + filename);//要文件的源地址
        OutputStream out = resp.getOutputStream();//通过字节流把文件读写到Serlvet的resp请求头
        try {
            byte[] buffer = new byte[1024];//该数组用来存入从输入文件中读取到的数据
            int numBytesRead;//用来存储每次读取数据后的返回值
            //read()这个方法完成的事情就是从数据源中读取8个二进制位，并将这8个0或1转换成十进制的整数，然后将其返回
            while ((numBytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, numBytesRead);
            }//while循环：每次从输入文件读取数据后，都写入到输出文件中
        } finally {
            out.close();
            in.close();
        }
    }

    @CrossOrigin
    @GetMapping("/cinemaall")
    public List<User> cinemaall(){
        return cinemaService.Userall();
    }

    @CrossOrigin
    @DeleteMapping("/cinemaidList")
    public boolean cinemaDelList( @RequestParam(value = "Userid",required = false) List<Integer> idList){
//        System.out.println(idList);
            return cinemaService.UserDelList(idList);
    }

    @CrossOrigin
    @GetMapping("/videoall")
    public List<allList> videoall(){
        return cinemaService.videoAll();
    }

    @CrossOrigin
    @GetMapping("/ss")
    public List<allList> videoall2(){
        return cinemaService.videoAll();
    }

    @CrossOrigin
    @PostMapping("/allListAdd")
    public boolean allListAdd(HttpServletRequest request) throws IOException, ServletException {
        String name = request.getParameter("name");
        String main = request.getParameter("main");
        String type = request.getParameter("type");
        String videotype = request.getParameter("videotype");
        String theme = request.getParameter("theme");
        String subheading = request.getParameter("subheading");
        String time = request.getParameter("time");
        String swift = request.getParameter("swift");

        Part tp = request.getPart("img");
        Part sp = request.getPart("video");
        if (tp==null){
            System.out.println("11111111111111");
        }
        String tpname = tp.getSubmittedFileName();
        String spname = sp.getSubmittedFileName();



        String newFileName = UUID.randomUUID()+tpname;
        String newFileName2 = UUID.randomUUID()+spname;


        String filePathtp = "D:\\cinema-stockpile\\imgs\\";
        new File(filePathtp).mkdir();
        tp.write(filePathtp+newFileName);
        String filePathsp = "D:\\cinema-stockpile\\mp4\\";
        new File(filePathsp).mkdir();
        sp.write(filePathsp+newFileName2);

        allList allList = new allList(name,newFileName,newFileName2,main,type,videotype,theme,subheading,time,swift);
        return cinemaService.allListAdd(allList);
    }
//    @CrossOrigin
//    @GetMapping("/cinemalim")
//    public List<allList> cinemalim(){
//        return cinemaService.cinemalim();
//    }

    @CrossOrigin
    @PostMapping("/cinemaUP")
    public boolean UserUP(User user){
        return cinemaService.UserUp(user);
    }

    @CrossOrigin
    @GetMapping("/adminlogin")
    public List<SuperAdmin> adminlogin(String User,Integer password){
        System.out.println(User+"-"+password);
        return cinemaService.adminlogin(User,password);
    }

    @CrossOrigin
    @GetMapping("/adminshow")
    public List<SuperAdmin> adminshow(){
        return cinemaService.adminshow();
    }

    @CrossOrigin
    @GetMapping("/allListid")
    public allList allListid(Integer cinemaid){
        System.out.println("视频id为"+cinemaid);
        return cinemaService.allListshow(cinemaid);
    }

    @CrossOrigin
    @GetMapping("/comment")
    public List<Comments> comment(){
        return cinemaService.comment();
    }

    @CrossOrigin
    @PostMapping("/commentplun")
    public boolean commentplun(Commentsplun commentsplun){
        return cinemaService.Commentplun(commentsplun);
    }

    @CrossOrigin
    @GetMapping("/alllistvideotype")
    public List<allList> alllistshowtype(String type,String videotype){
//        String ty = "%"+type+"%";
//        String videoty = "%"+videotype+"%";
        return cinemaService.alllistshowtype(type,videotype);
    }

    @CrossOrigin
    @GetMapping("/historyadd")
    public boolean history(history history){
        return cinemaService.history(history);
    }

    @CrossOrigin
    @GetMapping("/historyshow")
    public List<history2> historyshow(Integer Userid){
        return cinemaService.historyshow(Userid);
    }

    @CrossOrigin
    @PostMapping("/historyshow2")
    public List<history2> historyshow2(String Username,String name){
        System.out.println(Username+name);
        return cinemaService.historyshow2(Username,name);
    }

    @CrossOrigin
    @GetMapping("/historyshowdata")
    public List<history2> historyshowdata(){
        return cinemaService.historyshowdata();
    }

    @CrossOrigin
    @PostMapping("/cinemaallshow")
    public  List<allList> cinemaallshow(String name){
        return cinemaService.cinemaallshow(name);
    }
    @CrossOrigin
    @GetMapping("/plunid")
    public boolean plunid(Integer id){
        return cinemaService.plunid(id);
    }
    @CrossOrigin
    @GetMapping("/Userid")
    public User Userid(Integer Userid){
           return cinemaService.Userid(Userid);
    }
}
