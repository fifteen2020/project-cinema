
数据库名:ProjectCinema

#### User表(用户表)

| 字段名 | 类型 |长度|是否可为空|数据项含义
|---|---|---|---|---|
|Userid|int|主键、自增|否|序号|
|User|varchar|10|否|用户账号
|gender|varchar|4|否|用户性别
|email|varchar|50|否|邮箱|
|password|int||否|用户密码

随机添加3个用户，规范，简洁


#### SuperAdmin表(管理员表)

| 字段名 | 类型 |长度|是否可为空|数据项含义
|---|---|---|---|---|
|adminid|int|主键、自增|否|序号|
|User|varchar|10|否|管理员账号
|password|int||否|管理员密码

添加admin、SuperAdmin两个管理账号


#### allList表(电影表)
| 字段名 | 类型 |长度|是否可为空|数据项含义
|---|---|---|---|---|
|cinemaid|int|主键、自增|否|序号|
|name|varchar|100|否|名称|
|img|varchar|255|否|封面|
|video|varchar|255|否|视频|
|main|varchar|100|否|主演|
|type|varchar|50||类型|
|videotype|varchar|40|否|视频类型|
|theme|varchar|255||主标题|
|subheading|varchar|255||副标题|
|time|varchar|100||上映时间|
|swift|varchar|10|否|上下架|


#### cinematop表(电影榜单表)

| 字段名 | 类型 |长度|是否可为空|数据项含义
|---|---|---|---|---|
|topid|int|自增、主键|否|排名
|topname|varchar|50|否|电影名称|


#### zonyi(综艺榜单表)

| 字段名 | 类型 |长度|是否可为空|数据项含义
|---|---|---|---|---|
|zonyiid|int|自增、主键|否|排名
|zonyiname|varchar|50|否|综艺名称|
